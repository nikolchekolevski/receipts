$(document).ready(function(){
	var categories;
	var sentreceipt;
	$("#article").click(function(){
		categories = JSON.parse((JSON.parse($('#categories').val())).categories);
		var ul = document.getElementById("articles");
		var li = document.createElement("li");
		var input = document.createElement("input");
		input.setAttribute("name", "articleName");
		var input2 = document.createElement("input");
		input2.setAttribute("name", "amount");
		var input3 = document.createElement("input");
		input3.setAttribute("name", "price");
		
		var select = document.createElement("select");
		select.setAttribute("hidden","");
		var option = document.createElement("option");
		option.setAttribute("value", "blank");
		select.appendChild(option);
	    for(var i = 0; i < categories.length; ++i) {
			var option = document.createElement("option");
			option.text = categories[i].name;
			option.setAttribute("value", JSON.stringify(categories[i]));
			select.appendChild(option);
		}
	    	
	    	/*var ul = document.getElementById("uncatarticles");
	    	var li = document.createElement("li");
	    	var input = document.createElement("input");
			input.setAttribute("name", "articleName");
			input.value = datareceipt.receiptEntries[index].article.articleName;
			li.appendChild(input);
			li.appendChild(select);
	    	ul.appendChild(li);*/
		
		var span1 = document.createElement("span");
		span1.textContent = "article";
		var span4 = document.createElement("span");
		span1.textContent = "category";
		var span2 = document.createElement("span");
		span2.textContent = "amount";
		var span3 = document.createElement("span");
		span3.textContent = "price";
		li.appendChild(span1);
		li.appendChild(input);
		li.appendChild(span2);
		li.appendChild(input2);
		li.appendChild(span3);
		li.appendChild(input3);
		li.appendChild(span4);
		li.appendChild(select)
		ul.appendChild(li);
	});
    $("#add").click(function(){
    	var receipt = new Object();
    	receipt.shop = new Object();
        receipt.shop.name = $("#shopName").val();
    	receipt.receiptEntries=[];
    	$("#articles li").each(function(){
    		var receiptEntry = new Object();
    		receiptEntry.article = new Object();
    		receiptEntry.article.articleName = $(this).find("input[name='articleName']").val();
    		receiptEntry.amount = parseInt($(this).find("input[name='amount']").val());
    		receiptEntry.price = parseInt($(this).find("input[name='price']").val());
    		var category = $(this).find("select").val();
    		if(category != "blank"){
    			receiptEntry.article.category = JSON.parse(category);
    		}
    		receipt.receiptEntries.push(receiptEntry);
    	   });
    	sentreceipt = receipt;
       /*var receipt = new Object();
       receipt.shopName="sdf";
       receipt['totalAmount']=5;
       var ar = new Object();
       ar.articleName="sadhf";
       var tk = new Object();
       tk.articleName="ghsdg";
       receipt['articles']=[ar,tk];*/
       

       $.ajax({
       		  url: 'add',
       		  type: 'POST',
              contentType: 'application/json;charset=UTF-8',
              dataType: 'json',
              cache: false, 
              processData: false,
       		  data: JSON.stringify(receipt),
       		  success: function(data) {
       			var datareceipt = JSON.parse(data.receipt);
         		//categories = JSON.parse(data.categories);
       			
         		for (index = 0; index < datareceipt.receiptEntries.length; ++index) {
       			    if(datareceipt.receiptEntries[index].article.category==null){
       			    	$("#articles li").each(function(){
       	       	    		if(datareceipt.receiptEntries[index].article == $(this).find("input[name='articleName']").val());
       	       	    		$(this).find("select").removeAttr("hidden");
       	       	    	});	
       			    	/*var select = document.createElement("select");
       			    	for(var i = 0; i < categories.length; ++i) {
              				var option = document.createElement("option");
               				option.text = categories[i].name;
              				option.setAttribute("value", categories[i].id);
               				select.appendChild(option);
               			}
       			    	
       			    	var ul = document.getElementById("uncatarticles");
      			    	var li = document.createElement("li");
       			    	var input = document.createElement("input");
      					input.setAttribute("name", "articleName");
       					input.value = datareceipt.receiptEntries[index].article.articleName;
       					li.appendChild(input);
       					li.appendChild(select);
       			    	ul.appendChild(li);*/
       			    }
       			}
       			//.receiptEntries[1].article.category
       			$("#result").text(data.toString);
       		  },
       		  error: function(e) {
       			//called when there is an error
       			
       		  }
       	});
    });
    $("#categorize").click(function(){
    	var categorize = [];
    	$("#uncatarticles li").each(function(){
    		var receiptEntry = new Object();
    		receiptEntry.article = new Object();
    		receiptEntry.article.articleName = $(this).find("input[name='articleName']").val();
    		for(i = 0; i < sentreceipt.receiptEntries.length; ++i){
    			if(sentreceipt.receiptEntries[i].article.articleName === receiptEntry.article.articleName){
    				receiptEntry.amount=sentreceipt.receiptEntries[i].amount;
    				receiptEntry.price=sentreceipt.receiptEntries[i].price;
    			}
    		}
    		var id = parseInt($(this).find("select option:selected").val());
    		for(i = 0; i < categories.length; ++i){
    			if(categories[i].id === id){
    				receiptEntry.article.category = categories[i];
    			}
    		}
    		categorize.push(receiptEntry);
    	   });
       
       /*var receipt = new Object();
       receipt.shopName="sdf";
       receipt['totalAmount']=5;
       var ar = new Object();
       ar.articleName="sadhf";
       var tk = new Object();
       tk.articleName="ghsdg";
       receipt['articles']=[ar,tk];*/
       

       $.ajax({
       		  url: 'categorize',
       		  type: 'POST',
              contentType: 'application/json;charset=UTF-8',
              dataType: 'json',
              cache: false, 
              processData: false,
       		  data: JSON.stringify(categorize),
       		  success: function(data) {
       			
       		  },
       		  error: function(e) {
       			
       		  }
       	});
    });
});
