var app = angular.module('receiptApp', []);

/*
 * $(document).ready(function () { $('#img').imgAreaSelect({ handles: true,
 * onSelectChange: preview }); });
 */

app.controller('AddReceiptController', [
		'$scope',
		'$http',
		function($scope, $http) {
			$scope.title = "Add Receipt";
			$scope.ocr;
			$scope.showable = [];
			$scope.textmap = {
				shop : "Shop:",
				articles : "Articles:",
				date : "Date:",
				total : "Total:"
			};
			$scope.segment;
			$scope.newreceipt = {
				picture : '',
				shop : {
					name : ''
				},
				receiptEntries : [ {
					article : {
						articleName : '',
						category : {
							id : '',
							name : ''
						}
					},
					amount : '',
					price : ''
				} ]
			};

			$('#img').imgAreaSelect({
				handles : true,
				onSelectEnd : getOcr
			});

			function getOcr(img, selection) {
				var ratio = img.naturalWidth / img.offsetWidth;
				var receipt = {
					receipt : $scope.newreceipt,
					selection : {
						w : selection.width * ratio,
						h : selection.height * ratio,
						x : selection.x1 * ratio,
						y : selection.y1 * ratio
					},
					data : {
						segment : $scope.segment,
						textarea : $scope.textmap
					}
				}
				$.ajax({
					url : 'getOcr',
					type : 'POST',
					contentType : 'application/json;charset=UTF-8',
					dataType : 'json',
					cache : false,
					processData : false,
					data : JSON.stringify(receipt),
					success : function(data) {
						var textareamap = JSON.parse(data.ocr);
						$scope.textmap.shop = textareamap["Shop"];
						$scope.textmap.articles = textareamap["Articles"];
						$scope.textmap.date = textareamap["Date"];
						$scope.textmap.total = textareamap["Total"];
						$scope.ocr = $scope.textmap.shop + "\n"
								+ $scope.textmap.articles + "\n"
								+ $scope.textmap.date + "\n"
								+ $scope.textmap.total;
						$scope.newreceipt = JSON.parse(data.receipt);
						$scope.newreceipt.purchaseDate = new Date(
								$scope.newreceipt.purchaseDate);
						$scope.$apply();
					},
					error : function(e) {
					}
				});
				// alert("w"+selection.width+"h"+selection.height+"x"+selection.x1+"y"+selection.y1);
			}

			$scope.autoReceipt = function() {
				$scope.textmap.shop = $scope.ocr.substring($scope.ocr
						.indexOf("Shop:")
						+ "Shop:".length, $scope.ocr.indexOf("Articles:"));
				$scope.textmap.articles = $scope.ocr.substring($scope.ocr
						.indexOf("Articles:")
						+ "Articles:".length, $scope.ocr.indexOf("Date:"));
				$scope.textmap.date = $scope.ocr.substring($scope.ocr
						.indexOf("Date:")
						+ "Date:".length, $scope.ocr.indexOf("Total:"));
				$scope.textmap.total = $scope.ocr.substring($scope.ocr
						.indexOf("Total:")
						+ "Total:".length);
				var receipt = {
					receipt : $scope.newreceipt,
					data : $scope.textmap
				}
				$.ajax({
					url : 'autoReceipt',
					type : 'POST',
					contentType : 'application/json;charset=UTF-8',
					dataType : 'json',
					cache : false,
					processData : false,
					data : JSON.stringify(receipt),
					success : function(data) {
						var textareamap = JSON.parse(data.ocr);
						$scope.textmap.shop = textareamap["Shop"];
						$scope.textmap.articles = textareamap["Articles"];
						$scope.textmap.date = textareamap["Date"];
						$scope.textmap.total = textareamap["Total"];
						$scope.ocr = $scope.textmap.shop + "\n"
								+ $scope.textmap.articles + "\n"
								+ $scope.textmap.date + "\n"
								+ $scope.textmap.total;
						$scope.newreceipt = JSON.parse(data.receipt);
						$scope.newreceipt.purchaseDate = new Date(
								$scope.newreceipt.purchaseDate);
						$scope.$apply();
					},
					error : function(e) {
					}
				});
				$scope.showable=[];
				for (var i = 1; i <= $scope.newreceipt.receiptEntries.length; i++) {
					var article = {
						articleName : $("#" + (i-1) + " input[name='article']").val(),
						id:i
					};
					$.ajax({
						url : 'checkArticle',
						type : 'POST',
						contentType : 'application/json;charset=UTF-8',
						dataType : 'json',
						cache : false,
						processData : false,
						data : JSON.stringify(article),
						success : function(data) {
							var check = data.article;
							var id = JSON.parse(data.id);
							if (check == "null") {
								$scope.showable.push(id);
							}
						},
						error : function(e) {

						}
					});
				}
			}

			$scope.$watchCollection('showable', function() {
				for(var i=0; i<$scope.showable.length;i++){
					$("#" + $scope.showable[i] + " select").attr("class","form-control");
					$("#" + $scope.showable[i] + " select").removeAttr("hidden");
					$("#categoryHead").removeAttr("hidden");
				}
				$scope.$apply();
			});
			
			$scope.showabl = function(id) {
				for(var i=0; i<$scope.showable.length;i++){
					if(id==$scope.showable[i]){
						$("#" + id + " select").attr("class","form-control");
						return true;
					}
				}
				return false;
			}
			
			$http.get('fetchReceipt' + location.search).success(
					function(data) {
						if (data.receipt != "null") {
							$scope.newreceipt = JSON.parse(data.receipt);
							$scope.newreceipt.purchaseDate = new Date(
									$scope.newreceipt.purchaseDate);
							$scope.title = "Edit Receipt";
						}
					})

			$scope.checkArticle = function(id) {
				var article = {
					articleName : $("#" + id + " input[name='article']").val()
				};
				$.ajax({
					url : 'checkArticle',
					type : 'POST',
					contentType : 'application/json;charset=UTF-8',
					dataType : 'json',
					cache : false,
					processData : false,
					data : JSON.stringify(article),
					success : function(data) {
						var check = data.article;
						if (check == "null") {
							$("#" + id + " select").attr("class",
									"form-control");
							$("#" + id + " select").removeAttr("hidden");
							$("#categoryHead").removeAttr("hidden");
						}
					},
					error : function(e) {

					}
				});
			}

			$scope.ocrSegment = function(val) {
				$scope.segment = val;
			}

			$scope.autoFill = function() {
				var image = $("#image");
				if (this.files && this.files[0]) {
					var FR = new FileReader();
					FR.onload = function(e) {
						$('#img').attr("src", e.target.result);
						$('#base').text(e.target.result);
						$scope.newreceipt.picture = e.target.result;

						$.ajax({
							url : 'autoFill',
							type : 'POST',
							contentType : 'application/json;charset=UTF-8',
							dataType : 'json',
							cache : false,
							processData : false,
							data : JSON.stringify($scope.newreceipt),
							success : function(data) {
								$scope.ocr = JSON.parse(data.ocr);
								$scope.$apply();
							},
							error : function(e) {

							}
						});
					};
					FR.readAsDataURL(this.files[0]);
				}
			}

			$("#image").change($scope.autoFill);

			$scope.addEntry = function() {
				$scope.newreceipt.receiptEntries.push({
					article : {
						articleName : '',
						category : {
							id : '',
							name : ''
						}
					},
					amount : '',
					price : ''
				});
			};

			$scope.removeRow = function(id) {
				$scope.newreceipt.receiptEntries.splice(id, 1);
			};

			$scope.saveReceipt = function() {
				$.ajax({
					url : 'addReceipt',
					type : 'POST',
					contentType : 'application/json;charset=UTF-8',
					dataType : 'json',
					cache : false,
					processData : false,
					data : JSON.stringify($scope.newreceipt),
					success : function(data) {
						window.location = "receiptsList";
					},
					error : function(e) {

					}
				});
				/*
				 * console.log('you can save all the rows as a document: ');
				 * console.log($scope.rows); console.log('or save row by row:');
				 * var index = 0; $scope.rows.forEach(function (row) {
				 * console.log('row #' + (index++) + ': ' +
				 * JSON.stringify(row)); });
				 */
			}
		} ]);
