var app = angular.module('receiptApp',[]);

$( document ).ready(function() {
	$("#categoryFilter").prepend(new Option("", ""));
	$("#categoryFilter").val("").change();
});

app.controller('ReceiptsController', ['$scope', '$http',function($scope,$http) {
	$scope.receipts = [];
	$scope.calcreceipts = [];
	$scope.articles = [];
	$scope.receiptentries = [];
	$scope.selectedreceipt = {};
	$scope.selectedarticle = {};
	$scope.selectedIndex;
	$scope.selectedArticleIndex;
	$scope.fromAmount;
	$scope.toAmount;
	$scope.fromDate; 
	$scope.toDate; 
	$scope.shopName; 
	$scope.categorySearch = "";  
	$scope.showArticlesCheck = false;
	$scope.title = $scope.showArticlesCheck ? "Articles" : "Receipts";
		
    $scope.showReceipt = function(id) {
    	$scope.selectedreceipt = $scope.receipts[id];
    	$scope.selectedIndex = id;
    	$("#selected").removeAttr("hidden");
    	$("#recentries").attr("hidden");
    }
    
    $scope.showArticleDif = function(id) {
    	$scope.selectedarticle = $scope.articles[id];
    	$scope.selectedArticleIndex = id;
    	$("#selected").attr("hidden");
    	$("#recentries").removeAttr("hidden");
    	var query = "?article="+$scope.selectedarticle.article+"&purchaseDate="+$scope.selectedarticle.purchaseDate;
    	$http.get('getArticleDif'+query).
        success(function(data) {
        	$scope.receiptentries = [];
        	$scope.receiptentries = JSON.parse(data.receipt);
         })
    }
    
    
    $scope.editReceipt = function(id) {
    	window.location = "addReceipt?id="+id;
    }
	
	$http.get('fetchReceipts').
    success(function(data) {
         $scope.receipts = JSON.parse(data.receipts);
         $scope.calcreceipts = JSON.parse(data.receipts);
         for(var i=0;i<$scope.receipts.length;i++){
        	 if($scope.receipts[i].purchaseDate!=null){
        		 $scope.receipts[i].purchaseDate = new Date($scope.receipts[i].purchaseDate); 
        	 }
         }
     })
     
    
     
 	/*$scope.checkArticle = function(id) {
		var article = {articleName : $("#"+id+" input[name='article']").val()};
		$.ajax({
     		url: 'checkArticle',
     		type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json',
            cache: false, 
            processData: false,
     		  data: JSON.stringify(article),
     		  success: function(data) {
     			var check = data.article;
     			if(check=="null"){
     				$("#"+id+" select").removeAttr("hidden");
     			}
     		  },
     		  error: function(e) {
     			// called when there is an error
     			
     		  }
     	});
	}*/
	
	$scope.addEntry = function() {
		$scope.selectedreceipt.receiptEntries.push({
			article : {
				articleName : '',
				category : {
					id:'',
					name:''
				}
			},
			amount : '',
			price : ''
		});
	};
	
	/*$scope.removeRow = function(id) {
		$scope.selectedreceipt.receiptEntries.splice(id,1);
	};*/
	
	$scope.checkReceipt = function(receipt) {
		var check = true;
		if($scope.fromAmount!=undefined && $scope.fromAmount!=null && $scope.fromAmount!=""){
			check = check && receipt.totalAmount >= $scope.fromAmount; 
		}
		if($scope.toAmount!=undefined && $scope.toAmount!=null && $scope.toAmount!=""){
			check = check && receipt.totalAmount <= $scope.toAmount; 
		}
		if($scope.fromDate!=undefined && $scope.fromDate!=null){
			check = check && receipt.purchaseDate >= $scope.fromDate; 
		}
		if($scope.toDate!=undefined && $scope.toDate!=null){
			check = check && receipt.purchaseDate <= $scope.toDate; 
		}
		if($scope.shopName!=undefined && $scope.shopName!=null && $scope.shopName!=""){
			check = check && receipt.shop.name == $scope.shopName; 
		}
		
		$scope.calcpopulate(receipt,check);
		return check; 
	};
	
	$scope.checkArticle = function(i,j) {
		var check = true;
		if($scope.fromAmount!=undefined && $scope.fromAmount!=null && $scope.fromAmount!=""){
			check = check && $scope.receipts[i].receiptEntries[j].price >= $scope.fromAmount; 
		}
		if($scope.toAmount!=undefined && $scope.toAmount!=null && $scope.toAmount!=""){
			check = check && $scope.receipts[i].receiptEntries[j].price <= $scope.toAmount; 
		}
		if($scope.fromDate!=undefined && $scope.fromDate!=null){
			check = check && $scope.receipts[i].purchaseDate >= $scope.fromDate; 
		}
		if($scope.toDate!=undefined && $scope.toDate!=null){
			check = check && $scope.receipts[i].purchaseDate <= $scope.toDate; 
		}
		if($scope.shopName!=undefined && $scope.shopName!=null && $scope.shopName!=""){
			check = check && $scope.receipts[i].shop.name == $scope.shopName; 
		}
		if($scope.categorySearch!="" && $scope.categorySearch!=undefined && $scope.categorySearch!=null){
			check = check && $scope.receipts[i].receiptEntries[j].article.category.name==$scope.categorySearch;
		}
		return check;
	}
	
	$scope.calcpopulate = function(receipt,check){
		for(var i=0;i<$scope.calcreceipts.length;i++){
			if($scope.calcreceipts[i].id==receipt.id && check!=true){
				$scope.calcreceipts[i].totalAmount=0;
			}else if ($scope.calcreceipts[i].id==receipt.id){
				$scope.calcreceipts[i].totalAmount=$scope.receipts[i].totalAmount;
			}
        }
	}
	
	$scope.tableTotal = function() {
		var total = 0;
		for(var i=0;i<$scope.calcreceipts.length;i++){
			total+=$scope.calcreceipts[i].totalAmount;
        }
		return total
	};
		
	$scope.populateArticles = function() {
		$scope.articles = [];
		$scope.articleTotalAmount=0;
		$scope.articleTotal=0;
		for(var i=0; i<$scope.receipts.length;i++){
			for(var j=0; j<$scope.receipts[i].receiptEntries.length;j++){
				if( $scope.checkArticle(i,j)){
					$scope.articleTotalAmount+=$scope.receipts[i].receiptEntries[j].amount;
					$scope.articleTotal+=$scope.receipts[i].receiptEntries[j].price;
					addArticle(i,j);
				}
				/*if($scope.receipts[i].receiptEntries[j].article.category.name==$scope.categorySearch){
					$scope.articleTotalAmount+=$scope.receipts[i].receiptEntries[j].amount;
					$scope.articleTotal+=$scope.receipts[i].receiptEntries[j].price;
					addArticle(i,j);
				}else if($scope.categorySearch==""){
					$scope.articleTotalAmount+=$scope.receipts[i].receiptEntries[j].amount;
					$scope.articleTotal+=$scope.receipts[i].receiptEntries[j].price;
					addArticle(i,j);
				}*/
			}
		}
		
	}
	
	$scope.showReceipts = function(){
		$scope.categorySearch = "";
		$scope.showArticlesCheck = false;
		$scope.title = $scope.showArticlesCheck ? "Articles" : "Receipts";
	}
	
	$scope.showArticles = function(){
		$scope.showArticlesCheck = true;
		$("#selected").attr("hidden","");
		$scope.title = $scope.showArticlesCheck ? "Articles" : "Receipts";
		$scope.populateArticles();
	}
	
	addArticle =  function(i,j) {
		$scope.articles.push(
				{article:$scope.receipts[i].receiptEntries[j].article.articleName,
				amount:$scope.receipts[i].receiptEntries[j].amount,
			 	price:$scope.receipts[i].receiptEntries[j].price,
			 	purchaseDate:$scope.receipts[i].purchaseDate,
			 	shop:$scope.receipts[i].shop.name,
			 	category:$scope.receipts[i].receiptEntries[j].article.category.name
			}
		);
	}
	
	$scope.checkIfArticles = function(receipt) {
		return $scope.categorySearch!=undefined && $scope.categorySearch != null && $scope.showArticlesCheck;
	};
	
	$scope.deleteReceipt = function(id) {
		$scope.receipts.splice($scope.selectedIndex,1);
		$.ajax({
     		url: 'deleteReceipt',
     		type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json',
            cache: false, 
            processData: false,
     		  data: JSON.stringify($scope.selectedreceipt),
     		  success: function(data) {
     				$scope.selectedreceipt = {};
     				$("#selected").attr("hidden","");
     		  },
     		  error: function(e) {
     			// called when there is an error
     			
     		  }
     	});
	};
	
	/*$scope.saveReceipt = function() {
		$.ajax({
     		url: 'addReceipt',
     		type: 'POST',
            contentType: 'application/json;charset=UTF-8',
            dataType: 'json',
            cache: false, 
            processData: false,
     		  data: JSON.stringify($scope.selectedreceipt),
     		  success: function(data) {
     			var datareceipt = JSON.parse(data.receipt);
       		// categories = JSON.parse(data.categories);
     			for (index = 0; index < datareceipt.receiptEntries.length; ++index) {
     			    if(datareceipt.receiptEntries[index].article.category==null||datareceipt.receiptEntries[index].article.category.name==""){
     			    	$('select').removeAttr("hidden");
     			    }
     			}
     			// .receiptEntries[1].article.category
     			$("#result").text(data.toString);
     		  },
     		  error: function(e) {
     			// called when there is an error
     			
     		  }
     	});
	}*/
}]);
