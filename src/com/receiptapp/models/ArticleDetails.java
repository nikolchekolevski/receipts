package com.receiptapp.models;

public class ArticleDetails {

	private String shop;
	private ReceiptEntry entry;
	public ArticleDetails(String shop, ReceiptEntry entry) {
		this.setShop(shop);
		this.setEntry(entry);
	}
	public String getShop() {
		return shop;
	}
	public void setShop(String shop) {
		this.shop = shop;
	}
	public ReceiptEntry getEntry() {
		return entry;
	}
	public void setEntry(ReceiptEntry entry) {
		this.entry = entry;
	}
}
