package com.receiptapp.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Receipt {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name="SHOP_ID", nullable=false)
	private Shop shop;
	@OneToMany(targetEntity=com.receiptapp.models.ReceiptEntry.class,cascade={CascadeType.PERSIST,CascadeType.MERGE})
	private List<ReceiptEntry> receiptEntries;
	private int totalAmount;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date purchaseDate;
	private Customer customer;
	
	@Lob
	@Column(length = 5 * 1024 * 1024)
	private String picture;
	
	public Receipt() {
		// TODO Auto-generated constructor stub
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public List<ReceiptEntry> getReceiptEntries() {
		return receiptEntries;
	}
	public void setReceiptEntries(List<ReceiptEntry> receiptEntries) {
		this.receiptEntries = receiptEntries;
		int total = 0;
		for(ReceiptEntry receiptEntry : receiptEntries){
			total += receiptEntry.getPrice() * receiptEntry.getAmount();
		}
		setTotalAmount(total);		
	}
	public int getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	/*public Receipt() {
		articles = new ArrayList<Article>();
		totalAmount = 0;
	}*/
	

}
