package com.receiptapp.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String articleName;
	@ManyToOne
    @JoinColumn(name="CATEGORY_ID", nullable=false)
	private Category category;
	@OneToMany(targetEntity=com.receiptapp.models.ReceiptEntry.class, mappedBy="article",cascade=CascadeType.PERSIST)
	@JsonIgnore
	private List<ReceiptEntry> receiptEntries;
	
	public Article() {
	}
	
	public Article(String name, Category category) {
		setArticleName(name);
		setCategory(category);
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	
	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}

	
	public List<ReceiptEntry> getReceiptEntries() {
		return receiptEntries;
	}

	public void setReceiptEntries(List<ReceiptEntry> receiptEntries) {
		this.receiptEntries = receiptEntries;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
