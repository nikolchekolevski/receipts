package com.receiptapp.utils;

import com.receiptapp.models.Customer;
import com.receiptapp.services.CustomerService;

public class CustomerUtils {
	
	private static CustomerService customerService = new CustomerService();
	
	public static Customer getCustomerByName(String name) {
		return customerService.getCustomerByName(name);
	}
	
	public static Customer getCustomerByOauthId(String oauthId) {
		return customerService.getCustomerByOauthId(oauthId);
	}

	public static void addCustomer(Customer customer) {
		customerService.addCustomer(customer);
	}

}
