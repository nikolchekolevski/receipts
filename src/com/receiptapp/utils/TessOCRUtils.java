package com.receiptapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import com.receiptapp.models.Article;
import com.receiptapp.models.ReceiptEntry;
import com.receiptapp.services.TessOCRService;

public class TessOCRUtils {

	private static TessOCRService tessOCRService = new TessOCRService();

	public static String extractTextFromImage(String imageBase64, ServletContext servletContext) {
		return tessOCRService.extractTextFromImage(imageBase64, servletContext);
	}

	public static String getSegmentOCR(String picture, String width, String height, String xcord, String ycord,
			ServletContext servletContext) {
		return tessOCRService.getSegmentOcr(picture, width, height, xcord, ycord, servletContext);
	}

	public static Date parseDate(String ocr) {
		Date d = null;
		Pattern p = Pattern.compile("(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-]([0-9]{4})");
		Matcher m = p.matcher(ocr);
		if (m.find()) {
			try {
				d = new SimpleDateFormat("dd-MM-yyyy").parse(m.group());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return d;
	}

	public static Integer parseTotal(String ocr) {
		Integer total = null;
		Pattern p = Pattern.compile("([0-9]+[,.][0-9]{2})");
		Matcher m = p.matcher(ocr);
		if (m.find()) {
			String amount = m.group();
			total = Integer.parseInt(amount.substring(0, amount.length() - 3));
		}
		return total;
	}

	public static List<ReceiptEntry> parseEntries(String ocr) {
		Pattern p = Pattern.compile("([0-9]+[,.][0-9]{2})");
		Pattern p2 = Pattern.compile("([0-9]+[ ]*[х][ ]*[0-9]+[,.][0-9]{2})");
		String[] ocrsegmentsall = ocr.split("\n");
		List<String> ocrsegments = new ArrayList<>();
		List<ReceiptEntry> entries = new ArrayList<>();
		for (int i = 0; i < ocrsegmentsall.length; i++) {
			String segment = ocrsegmentsall[i];
			if (!segment.isEmpty()) {
				ocrsegments.add(segment);
			}
		}
		for (int i = 0; i < ocrsegments.size(); i++) {
			ReceiptEntry entry = new ReceiptEntry();
			Article article = new Article();
			String articlename = "";
			String segment = ocrsegments.get(i);
			String nextsegment = "";
			if (i < ocrsegments.size() - 1) {
				nextsegment = ocrsegments.get(i + 1);
			}
			Matcher m = p.matcher(segment);
			Matcher m2 = p2.matcher(segment);
			Matcher m3 = p2.matcher(nextsegment);
			Matcher m4 = p.matcher(nextsegment);
			if (m2.find()) {
				String check = segment.substring(0, m2.start());
				articlename += check;
				if (!m3.find() && m4.find()) {
					articlename+=nextsegment.substring(0, m4.start());
					i++;
				}
				article.setArticleName(articlename);
				entry.setArticle(article);

				String multiplier = m2.group();
				String[] pieces = multiplier.split("х");
				entry.setAmount(Integer.parseInt(pieces[0].replace(" ", "")));
				String price = pieces[1].replaceAll(" ", "");
				entry.setPrice(Integer.parseInt(price.substring(0, price.length() - 3)));
				entries.add(entry);
			} else if (m.find()) {
				articlename += segment.substring(0, m.start());
				article.setArticleName(articlename);
				entry.setArticle(article);

				String multiplier = m.group();
				entry.setAmount(1);
				entry.setPrice(Integer.parseInt(multiplier.substring(0, multiplier.length() - 3)));
				entries.add(entry);
			} else {
				articlename += segment;
				if (!m3.find() && m4.find()) {
					articlename += nextsegment.substring(0, m4.start());
					i++;
				}
				article.setArticleName(articlename);
				entry.setArticle(article);

				String multiplier = m4.group();
				entry.setAmount(1);
				entry.setPrice(Integer.parseInt(multiplier.substring(0, multiplier.length() - 3)));
				entries.add(entry);
			}
		}
		return entries;
	}
}
