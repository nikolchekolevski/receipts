package com.receiptapp.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.receiptapp.models.Article;
import com.receiptapp.models.Receipt;
import com.receiptapp.models.ReceiptEntry;
import com.receiptapp.services.ArticleService;
import com.receiptapp.services.ReceiptEntryService;;

public class ReceiptEntryUtils {
	
	private static ArticleService articleService = new ArticleService();
	private static ReceiptEntryService receiptEntryService = new ReceiptEntryService();

	public static void checkArticle(ReceiptEntry re) {
		Article article = articleService.getArticleByName(re.getArticle().getArticleName());
		if(article!=null){
			re.setArticle(article);
		}
	}

	public static List<Receipt> getReceiptDif(String article, Date date) {
		if(date!=null){
			return receiptEntryService.getReceiptDif(article, date);
		}
		return new ArrayList<Receipt>();
	}
}
