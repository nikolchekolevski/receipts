package com.receiptapp.utils;

import java.util.List;

import com.receiptapp.models.Customer;
import com.receiptapp.models.Receipt;
import com.receiptapp.models.ReceiptEntry;
import com.receiptapp.services.ArticleService;
import com.receiptapp.services.ReceiptService;

public class ReceiptUtils {

	private static ReceiptService receiptService = new ReceiptService();
	
	public static void checkArticles(Receipt receipt) {
		for(ReceiptEntry re : receipt.getReceiptEntries()){
			ReceiptEntryUtils.checkArticle(re);
		}
	}

	public static void addReceipt(Receipt receipt) {
		receiptService.addReceipt(receipt);
	}

	public static List<Receipt> getReceipts() {
		return receiptService.getReceipts();
	}

	public static void setCategories(Receipt receipt) {
		for(ReceiptEntry re : receipt.getReceiptEntries()){
			ArticleUtils.setCategory(re);
		}
	}

	public static void editReceipt(Receipt receipt) {
		receiptService.editReceipt(receipt);
	}

	public static boolean checkReceiptById(int id) {
		Receipt receipt = receiptService.getReceiptById(id);
		if(receipt!=null){
			return true;
		}
		return false;
	}

	public static void deleteReceipt(int id) {
		receiptService.deleteReceipt(id);
	}

	public static List<Receipt> getReceiptsForCostumer(Customer customer) {
		return receiptService.getReceiptsForCostumer(customer.getId());
	}

	public static Receipt getReceiptById(Integer id) {
		return receiptService.getReceiptById(id);
	}
}
