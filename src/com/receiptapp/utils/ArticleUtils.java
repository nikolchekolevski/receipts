package com.receiptapp.utils;

import com.receiptapp.models.Article;
import com.receiptapp.models.Category;
import com.receiptapp.models.ReceiptEntry;
import com.receiptapp.services.ArticleService;

public class ArticleUtils {

	private static ArticleService articleService = new ArticleService();
	
	public static Article checkIfNewArticle(String articleName) {
		return articleService.getArticleByName(articleName);
	}

	public static void addArticle(String name, Category category) {
		articleService.addArticle(new Article(name,category));		
	}

	public static void setCategory(ReceiptEntry re) {
		Category category = CategoryUtils.getCategoryByName(re.getArticle().getCategory().getName());
		if(category!=null){
			(re.getArticle()).setCategory(category);
		}
	}
}
