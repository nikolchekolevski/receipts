package com.receiptapp.utils;

import java.util.List;

import com.receiptapp.models.Category;
import com.receiptapp.services.CategoryService;

public class CategoryUtils {

	
	private static CategoryService categoryService = new CategoryService();;
	
	public static List<Category> getCategories() {
		return categoryService.getCategories();
	}

	public static void addCategory(String name) {
		categoryService.addCategory(new Category(name));		
	}

	public static Category getCategoryByName(String name) {
		return categoryService.getCategoryByName(name);
	}


}
