package com.receiptapp.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.receiptapp.models.Receipt;


@Service
public class ReceiptService {
	private static final String PERSISTENCE_UNIT_NAME = "receipt";
	private EntityManagerFactory factory;
	EntityManager em;

	public ReceiptService() {
		factory = Persistence
				.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}
	
	@SuppressWarnings("unchecked")
	public List<Receipt> getReceipts() {
		Query q = em.createQuery("select r from Receipt r");
		List<Receipt> receipts = q.getResultList();
		return receipts;
	}
	
	public Receipt getReceiptById(int id) {
		Receipt receipt = (Receipt) em.find(Receipt.class, id);
		return receipt;
	}
	
	public Integer deleteReceipt(int id) {
		Receipt receipt = (Receipt) em.find(Receipt.class, id);
		em.getTransaction().begin();
		em.remove(receipt);
		em.getTransaction().commit();
		return null;
	}
	
	public Integer addReceipt(Receipt receipt) {
		Receipt sentinel = (Receipt) em
				.find(Receipt.class, receipt.getId());
		if (sentinel == null) {
			em.getTransaction().begin();
			em.persist(receipt);
			em.getTransaction().commit();
		} 
		return 1;
	}
	
	public Integer editReceipt(Receipt receipt) {
		  em.getTransaction().begin();
		  em.merge(receipt);
		  em.getTransaction().commit();
		  return 1;
	}

	public List<Receipt> getReceiptsForCostumer(int id) {
		Query q = em.createQuery("select r from Receipt r where r.customer.id = :id");
		q.setParameter("id", id);
		List<Receipt> receipts = q.getResultList();
		return receipts;
	}
}
