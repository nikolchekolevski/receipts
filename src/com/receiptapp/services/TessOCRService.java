package com.receiptapp.services;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;

import net.sourceforge.tess4j.ITessAPI.TessBaseAPI;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.Tesseract1;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;
import sun.misc.BASE64Decoder;

public class TessOCRService {
	
	public TessOCRService() {
		// TODO Auto-generated constructor stub
	}
	
	public String extractTextFromImage(String imageBase64, ServletContext servletContext){
				
		BASE64Decoder decoder = new BASE64Decoder();
        byte[] decodedBytes;
        BufferedImage image=null;
		try {
			decodedBytes = decoder.decodeBuffer(trim(imageBase64));
			image = ImageIO.read(new ByteArrayInputStream(decodedBytes));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	    Tesseract instance = Tesseract.getInstance();
	    
	    File tessDataFolder =  new File("/tessdata");// Maven build bundles English data
        instance.setDatapath(tessDataFolder.getAbsolutePath());


    	
        File tessdata = new File(servletContext.getRealPath("/tessdata"));
        instance.setDatapath(tessdata.getAbsolutePath());
        
        instance.setConfigs(Arrays.asList("TestConfig"));
        instance.setTessVariable("tessedit_char_whitelist", "0123456789АБВГДЃЕЖЗЅИЈКЛЉМНЊОПРСТЌУФХЦЧЏШабвгдѓежзѕијклљмнњопрстќуфхцчџш.,-#:%=");
        instance.setTessVariable("tessedit_zero_rejection", "0");
        instance.setTessVariable("load_system_dawg ", "false");
        instance.setTessVariable("load_freq_dawg", "false");
        
        instance.setLanguage("fsm");
	    String result = null;
	    try {
	        result = instance.doOCR(image);
	    } catch (TesseractException e) {
	        System.err.println(e.getMessage());
	    }
	    System.out.println(result);
		return result;
	}

	private String trim(String imageBase64) {
		String cut = "data:image/jpeg;base64,";
		System.out.println(imageBase64.substring(imageBase64.indexOf(cut)+cut.length()));
		return imageBase64.substring(imageBase64.indexOf(cut)+cut.length());
	}

	public String getSegmentOcr(String picture, String width, String height, String xcord, String ycord,
			ServletContext servletContext) {
		BASE64Decoder decoder = new BASE64Decoder();
        byte[] decodedBytes;
        BufferedImage image=null;
		try {
			decodedBytes = decoder.decodeBuffer(trim(picture));
			image = ImageIO.read(new ByteArrayInputStream(decodedBytes));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		BufferedImage small = image.getSubimage(conv(xcord),conv(ycord),conv(width),conv(height));
		
	    Tesseract instance = Tesseract.getInstance();
	    
	    File tessDataFolder =  new File("/tessdata");// Maven build bundles English data
        instance.setDatapath(tessDataFolder.getAbsolutePath());


    	
        File tessdata = new File(servletContext.getRealPath("/tessdata"));
        instance.setDatapath(tessdata.getAbsolutePath());
        
        instance.setConfigs(Arrays.asList("TestConfig"));
        instance.setTessVariable("tessedit_char_whitelist", "0123456789АБВГДЃЕЖЗЅИЈКЛЉМНЊОПРСТЌУФХЦЧЏШабвгдѓежзѕијклљмнњопрстќуфхцчџш.,-#:%=");
        instance.setTessVariable("tessedit_zero_rejection", "0");
        instance.setTessVariable("load_system_dawg ", "false");
        instance.setTessVariable("load_freq_dawg", "false");
        
        instance.setLanguage("fsm");
	    String result = null;
	    try {
	        result = instance.doOCR(small);
	    } catch (TesseractException e) {
	        System.err.println(e.getMessage());
	    }
	    System.out.println(result);
		return result;
	}

	private int conv(String val) {
		return (int)(Double.parseDouble(val));
	}

}
