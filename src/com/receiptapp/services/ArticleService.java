package com.receiptapp.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.receiptapp.models.Article;


@Service
public class ArticleService {

	private static final String PERSISTENCE_UNIT_NAME = "receipt";
	private EntityManagerFactory factory;
	EntityManager em;

	public ArticleService() {
		factory = Persistence
				.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}
	
	@SuppressWarnings("unchecked")
	public List<Article> getArticles() {
		Query q = em.createQuery("select a from Article a");
		List<Article> articles = q.getResultList();
		return articles;
	}
	
	public Article getArticleById(int id) {
		Article article = (Article) em.find(Article.class, id);
		return article;
	}
	
	public Integer deleteArticle(int id) {
		Article article = (Article) em.find(Article.class, id);
		em.getTransaction().begin();
		em.remove(article);
		em.getTransaction().commit();
		return null;
	}
	
	public Integer addArticle(Article article) {
		Article sentinel = (Article) em
				.find(Article.class, article.getId());
		if (sentinel == null) {
			em.getTransaction().begin();
			em.persist(article);
			em.getTransaction().commit();
		} 
		return 1;
	}

	@SuppressWarnings("unchecked")
	public Article getArticleByName(String articleName) {
		List<Article> articles = em.createQuery(
						"SELECT a FROM Article a where a.articleName = :articleName")
				.setParameter("articleName", articleName).getResultList();
		if (articles!=null && !articles.isEmpty()) {
			return articles.get(0);
		} else {
			return null;
		}
	}
}
