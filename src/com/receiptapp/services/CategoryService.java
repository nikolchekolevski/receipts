package com.receiptapp.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.receiptapp.models.Category;

@Service
public class CategoryService {

	private static final String PERSISTENCE_UNIT_NAME = "receipt";
	private EntityManagerFactory factory;
	EntityManager em;

	public CategoryService() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<Category> getCategories() {
		Query q = em.createQuery("select c from Category c");
		List<Category> categories = q.getResultList();
		return categories;
	}

	public Category getCategoryById(int id) {
		Category category = (Category) em.find(Category.class, id);
		return category;
	}

	public Integer deleteCategory(int id) {
		Category category = (Category) em.find(Category.class, id);
		em.getTransaction().begin();
		em.remove(category);
		em.getTransaction().commit();
		return null;
	}

	@SuppressWarnings("unchecked")
	public Integer addCategory(Category category) {
		//Category sentinel = (Category) em.find(Category.class, category.getId());
		List<Category> sentinel = em.createQuery("SELECT c FROM Category c where c.name = :name")
				.setParameter("name", category.getName()).getResultList();
		if (sentinel != null && sentinel.size() == 0) {
			em.getTransaction().begin();
			em.persist(category);
			em.getTransaction().commit();
		}
		return 1;
	}

	@SuppressWarnings("unchecked")
	public Category getCategoryByName(String name) {
		List<Category> categories = em.createQuery("SELECT c FROM Category c where c.name = :name")
				.setParameter("name", name).getResultList();
		if (categories != null && !categories.isEmpty()) {
			return categories.get(0);
		} else {
			return null;
		}
	}
}
