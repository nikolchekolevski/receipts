package com.receiptapp.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.receiptapp.models.Shop;

@Service
public class ShopService {

	private static final String PERSISTENCE_UNIT_NAME = "receipt";
	private EntityManagerFactory factory;
	EntityManager em;

	public ShopService() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<Shop> getShops() {
		Query q = em.createQuery("select s from Shop s");
		List<Shop> shops = q.getResultList();
		return shops;
	}

	public Shop getShopById(int id) {
		Shop shop = (Shop) em.find(Shop.class, id);
		return shop;
	}

	public Integer deleteShop(int id) {
		Shop shop = (Shop) em.find(Shop.class, id);
		em.getTransaction().begin();
		em.remove(shop);
		em.getTransaction().commit();
		return null;
	}

	@SuppressWarnings("unchecked")
	public Integer addShop(Shop shop) {
		/*
		 * Shop sentinel = (Shop) em .find(Shop.class, shop.getId());
		 */
		List<Shop> sentinel = em.createQuery("SELECT s FROM Shop s where s.name = :name")
				.setParameter("name", shop.getName()).getResultList();
		if (sentinel != null && sentinel.size() == 0) {
			em.getTransaction().begin();
			em.persist(shop);
			em.getTransaction().commit();
		}
		return 1;
	}
}
