package com.receiptapp.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.receiptapp.models.Customer;

@Service
public class CustomerService {
	private static final String PERSISTENCE_UNIT_NAME = "receipt";
	private EntityManagerFactory factory;
	EntityManager em;

	public CustomerService() {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getCustomers() {
		Query q = em.createQuery("select c from Customer c");
		List<Customer> customers = q.getResultList();
		return customers;
	}

	public Customer getCustomerById(int id) {
		Customer customer = (Customer) em.find(Customer.class, id);
		return customer;
	}

	public Integer deleteCustomer(int id) {
		Customer customer = (Customer) em.find(Customer.class, id);
		em.getTransaction().begin();
		em.remove(customer);
		em.getTransaction().commit();
		return null;
	}

	@SuppressWarnings("unchecked")
	public Integer addCustomer(Customer customer) {
		//Customer sentinel = (Customer) em.find(Customer.class, customer.getId());
		List<Customer> sentinel = em.createQuery("SELECT c FROM Customer c where c.name = :name")
				.setParameter("name", customer.getName()).getResultList();
		if (sentinel != null && sentinel.size() == 0) {
			em.getTransaction().begin();
			em.persist(customer);
			em.getTransaction().commit();
		}
		return 1;
	}

	@SuppressWarnings("unchecked")
	public Customer getCustomerByName(String name) {
		List<Customer> customers = em.createQuery("SELECT c FROM Customer c where c.name = :name")
				.setParameter("name", name).getResultList();
		if (customers != null && !customers.isEmpty()) {
			return customers.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Customer getCustomerByOauthId(String oauthId) {
		List<Customer> customers = em.createQuery("SELECT c FROM Customer c where c.oauthId = :oauthId")
				.setParameter("oauthId", oauthId).getResultList();
		if (customers != null && !customers.isEmpty()) {
			return customers.get(0);
		} else {
			return null;
		}
	}
}
