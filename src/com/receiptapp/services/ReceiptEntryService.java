package com.receiptapp.services;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.receiptapp.models.Receipt;
import com.receiptapp.models.ReceiptEntry;



@Service
public class ReceiptEntryService {
	private static final String PERSISTENCE_UNIT_NAME = "receipt";
	private EntityManagerFactory factory;
	EntityManager em;

	public ReceiptEntryService() {
		factory = Persistence
				.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = factory.createEntityManager();
	}
	
	@SuppressWarnings("unchecked")
	public List<ReceiptEntry> getReceiptEntries() {
		Query q = em.createQuery("select r from ReceiptEntry r");
		List<ReceiptEntry> receiptEntries = q.getResultList();
		return receiptEntries;
	}
	
	@SuppressWarnings("unchecked")
	public List<Receipt> getReceiptDif(String article, Date date) {
		Query q = em.createQuery("select r from Receipt r where r.purchaseDate=:date");
		
		//q.setParameter("article", article);
		q.setParameter("date", date);
		List<Receipt> receiptEntries = q.getResultList();
		return receiptEntries;
		
		/*Query q = em.createQuery("select r from Receipt r where r.customer.id = :id");
		q.setParameter("id", id);
		List<Receipt> receipts = q.getResultList();
		return receipts;*/
	}
	
	public ReceiptEntry getReceiptEntryById(int id) {
		ReceiptEntry receipt = (ReceiptEntry) em.find(ReceiptEntry.class, id);
		return receipt;
	}
	
	public Integer deleteReceiptEntry(int id) {
		ReceiptEntry receiptEntry = (ReceiptEntry) em.find(ReceiptEntry.class, id);
		em.getTransaction().begin();
		em.remove(receiptEntry);
		em.getTransaction().commit();
		return null;
	}
	
	public Integer addReceiptEntry(ReceiptEntry receiptEntry) {
		ReceiptEntry sentinel = (ReceiptEntry) em
				.find(ReceiptEntry.class, receiptEntry.getId());
		if (sentinel == null ) {
			em.getTransaction().begin();
			em.persist(receiptEntry);
			em.getTransaction().commit();
		} 
		return 1;
	}
}
