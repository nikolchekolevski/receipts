package com.receiptapp.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.receiptapp.models.Customer;
import com.receiptapp.utils.CustomerUtils;
import com.receiptapp.utils.GoogleAuthHelper;

@Controller
public class CustomerController {

	GoogleAuthHelper helper = new GoogleAuthHelper();

	@RequestMapping(value = "/register")
	public String register(Model model, HttpSession session) {
		model.addAttribute("google", helper.buildLoginUrl());
		return "register";
	}

	@RequestMapping(value = "/login")
	public String login(Model model, HttpSession session) {
		model.addAttribute("google", helper.buildLoginUrl());
		return "login";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String catadd(Customer customer, Model model, HttpServletRequest request, HttpSession session) {
		CustomerUtils.addCustomer(customer);
		session.setAttribute("customer", customer);
		model.addAttribute("customer", customer);
		return "redirect:" + "/receiptsList";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(Customer logcustomer, Model model, HttpServletRequest request, HttpSession session) {
		Customer customer = CustomerUtils.getCustomerByName(logcustomer.getName());
		session.setAttribute("customer", customer);
		model.addAttribute("customer", customer);
		return "redirect:" + "/receiptsList";
	}

	@RequestMapping(value = "/logout")
	public String login(Model model, HttpServletRequest request, HttpSession session) {
		session.removeAttribute("customer");
		return "redirect:" + "/receiptsList";
	}

	@RequestMapping(value = "/ret")
	public String oauth(Model model, HttpServletRequest request, HttpSession session) {
		String userInfo;
		try {
			userInfo = helper.getUserInfoJson(request.getParameter("code"));
			ObjectMapper mapper = new ObjectMapper();
			JsonNode json = mapper.readTree(userInfo);
			String name = json.get("name").asText();
			String oauthId = json.get("id").asText();
			

			Customer customer = CustomerUtils.getCustomerByOauthId(oauthId);
			if (customer == null) {
				customer = new Customer(name, oauthId);
				CustomerUtils.addCustomer(customer);
			}
			session.setAttribute("customer", customer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:" + "/receiptsList";
	}
}
