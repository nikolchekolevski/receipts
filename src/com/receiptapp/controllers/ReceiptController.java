package com.receiptapp.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.receiptapp.models.Article;
import com.receiptapp.models.ArticleDetails;
import com.receiptapp.models.Category;
import com.receiptapp.models.Customer;
import com.receiptapp.models.Receipt;
import com.receiptapp.models.ReceiptEntry;
import com.receiptapp.utils.ArticleUtils;
import com.receiptapp.utils.CategoryUtils;
import com.receiptapp.utils.ReceiptEntryUtils;
import com.receiptapp.utils.ReceiptUtils;
import com.receiptapp.utils.TessOCRUtils;

@Controller
public class ReceiptController {

	@RequestMapping(value = "/")
	public String styles() {
		return "redirect:" + "/receiptsList";
	}

	@RequestMapping(value = "/receiptsList")
	public String receiptsList(Model model, HttpSession session) {
		prepPage(model, session);
		return "receiptsList";
	}

	@RequestMapping(value = "/addReceipt")
	public String addReceipt(Model model, HttpSession session) {
		prepPage(model, session);
		return "addReceipt";
	}

	public void prepPage(Model model, HttpSession session) {
		Customer customer = (Customer) session.getAttribute("customer");
		model.addAttribute("customer", customer);
		List<Category> categories = new ArrayList<Category>();
		categories.addAll(CategoryUtils.getCategories());
		model.addAttribute("categories", categories);
	}

	@RequestMapping(value = "/checkArticle", method = RequestMethod.POST)
	@ResponseBody
	public String addReceipt(@RequestBody Article article, Model model, HttpServletRequest request,
			HttpSession session) {
		ObjectMapper mapper = new ObjectMapper();
		JsonObject result = null;
		try {
			if(article.getId()!=0){
				result = Json.createObjectBuilder().add("id", mapper.writeValueAsString(article.getId()-1))
					.add("article", mapper.writeValueAsString(ArticleUtils.checkIfNewArticle(article.getArticleName())))
					.build();
			}else{
				result = Json.createObjectBuilder()
						.add("article", mapper.writeValueAsString(ArticleUtils.checkIfNewArticle(article.getArticleName())))
						.build();
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	@RequestMapping(value = "/addReceipt", method = RequestMethod.POST)
	@ResponseBody
	public String addReceipt(@RequestBody Receipt receipt, Model model, HttpServletRequest request,
			HttpSession session) {
		Customer customer = (Customer) session.getAttribute("customer");
		if (customer != null) {
			receipt.setCustomer(customer);
		}

		/*
		 * if (!image.isEmpty()) { try { File convFile = new
		 * File(image.getOriginalFilename()); image.transferTo(convFile);
		 * 
		 * BufferedImage img = ImageIO.read(convFile); ByteArrayOutputStream
		 * baos = new ByteArrayOutputStream(); ImageIO.write(img, "png", baos);
		 * byte[] res = baos.toByteArray(); String encodedImage =
		 * Base64Utils.encodeToString(baos .toByteArray());
		 * receipt.setPicture(encodedImage); } catch (IOException e) {
		 * e.printStackTrace(); } }
		 */

		ReceiptUtils.checkArticles(receipt);
		boolean articlesMissing = false;
		for (ReceiptEntry re : receipt.getReceiptEntries()) {

			if (re.getArticle().getCategory() == null || re.getArticle().getCategory().getName().equals("")) {
				articlesMissing = true;
				break;
			}
		}

		if (!articlesMissing) {
			ReceiptUtils.setCategories(receipt);
			if (ReceiptUtils.checkReceiptById(receipt.getId())) {
				receipt.setReceiptEntries(receipt.getReceiptEntries());
				ReceiptUtils.editReceipt(receipt);
			} else {
				ReceiptUtils.addReceipt(receipt);
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("receipt", mapper.writeValueAsString(receipt)).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result.toString();
	}

	@RequestMapping(value = "/fetchReceipts")
	@ResponseBody
	public String fetchReceipts(Model model, HttpSession session) {
		Customer customer = (Customer) session.getAttribute("customer");
		List<Receipt> receipts = new ArrayList<Receipt>();
		if (customer != null) {
			receipts = ReceiptUtils.getReceiptsForCostumer(customer);
		}
		ObjectMapper mapper = new ObjectMapper();
		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("receipts", mapper.writeValueAsString(receipts)).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result.toString();
	}

	@RequestMapping(value = "/getArticleDif")
	@ResponseBody
	public String getArticleDif(@RequestParam("article") String article,
			@RequestParam("purchaseDate") String dateString, Model model, HttpSession session) {
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss", Locale.ENGLISH);
		dateString = dateString.substring(0, dateString.indexOf("GMT") - 1);
		try {
			date = formatter.parse(dateString);
			System.out.println(date);
			System.out.println(formatter.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<ArticleDetails> resultList = new ArrayList<ArticleDetails>();
		List<Receipt> entries = ReceiptEntryUtils.getReceiptDif(article, date);
		for (Receipt r : entries) {
			for (ReceiptEntry re : r.getReceiptEntries()) {
				if (re.getArticle().getArticleName().equals(article)) {
					resultList.add(new ArticleDetails(r.getShop().getName(), re));
				}
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("receipt", mapper.writeValueAsString(resultList)).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result.toString();
	}

	@RequestMapping(value = "/fetchReceipt")
	@ResponseBody
	public String fetchReceipt(@RequestParam("id") Integer id, Model model, HttpSession session) {
		Customer customer = (Customer) session.getAttribute("customer");
		Receipt receipt = null;
		if (customer != null) {
			receipt = ReceiptUtils.getReceiptById(id);
		}
		ObjectMapper mapper = new ObjectMapper();
		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("receipt", mapper.writeValueAsString(receipt)).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result.toString();
	}

	@RequestMapping(value = "/category")
	public String category(Model model, HttpSession session) {
		return "categories";
	}

	@RequestMapping(value = "/catadd", method = RequestMethod.POST)
	public String catadd(Category cat, Model model, HttpServletRequest request, HttpSession session) {
		CategoryUtils.addCategory(cat.getName());
		return "categories";
	}

	@RequestMapping(value = "/deleteReceipt", method = RequestMethod.POST)
	@ResponseBody
	public String deleteReceipt(@RequestBody Receipt receipt, Model model, HttpServletRequest request,
			HttpSession session) {
		ReceiptUtils.deleteReceipt(receipt.getId());
		return "";
	}

	@RequestMapping(value = "/getOcr", method = RequestMethod.POST)
	@ResponseBody
	public String getOcr(@RequestBody String data, Model model, HttpServletRequest request, HttpSession session) {
		// String ocr =
		// TessOCRUtils.extractTextFromImage(data.getPicture(),request.getSession().getServletContext());
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JsonNode input = null;
		try {
			input = mapper.readTree(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Map<String, String> textareamap = new HashMap<>();
		textareamap.put("Shop", "Shop:");
		textareamap.put("Articles", "Articles:");
		textareamap.put("Date", "Date:");
		textareamap.put("Total", "Total:");

		String picture = input.get("receipt").get("picture").asText();
		String width = input.get("selection").get("w").asText();
		String height = input.get("selection").get("h").asText();
		String xcord = input.get("selection").get("x").asText();
		String ycord = input.get("selection").get("y").asText();
		textareamap.put("Shop", input.get("data").get("textarea").get("shop").asText());
		textareamap.put("Articles", input.get("data").get("textarea").get("articles").asText());
		textareamap.put("Date", input.get("data").get("textarea").get("date").asText());
		textareamap.put("Total", input.get("data").get("textarea").get("total").asText());
		String segment = input.get("data").get("segment").asText();

		String ocr = TessOCRUtils.getSegmentOCR(picture, width, height, xcord, ycord,
				request.getSession().getServletContext());
		Receipt receipt = new Receipt();
		JsonNode rece = input.get("receipt");
		receipt = mapper.convertValue(rece, Receipt.class);

		JsonObject obj = null;

		if (segment.equals("Shop")) {
			textareamap.put("Shop", textareamap.get("Shop") + ocr);
			receipt.getShop().setName(ocr);
		} else if (segment.equals("Articles")) {
			textareamap.put("Articles", textareamap.get("Articles") + ocr);
			List<ReceiptEntry> re = TessOCRUtils.parseEntries(ocr);
			if (re != null) {
				receipt.setReceiptEntries(re);
			}
		} else if (segment.equals("Date")) {
			textareamap.put("Date", textareamap.get("Date") + ocr);
			Date d = TessOCRUtils.parseDate(ocr);
			if (d != null) {
				receipt.setPurchaseDate(d);
			}
		} else if (segment.equals("Total")) {
			textareamap.put("Total", textareamap.get("Total") + ocr);
			Integer total = TessOCRUtils.parseTotal(ocr);
			if (total != null) {
				receipt.setTotalAmount(total);
			}
		}

		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("ocr", mapper.writeValueAsString(textareamap))
					.add("receipt", mapper.writeValueAsString(receipt)).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result.toString();
	}
	

	@RequestMapping(value = "/autoReceipt", method = RequestMethod.POST)
	@ResponseBody
	public String autoReceipt(@RequestBody String data, Model model, HttpServletRequest request, HttpSession session) {
		// String ocr =
		// TessOCRUtils.extractTextFromImage(data.getPicture(),request.getSession().getServletContext());
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JsonNode input = null;
		try {
			input = mapper.readTree(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Map<String, String> textareamap = new HashMap<>();
		textareamap.put("Shop", "Shop:");
		textareamap.put("Articles", "Articles:");
		textareamap.put("Date", "Date:");
		textareamap.put("Total", "Total:");

		textareamap.put("Shop",input.get("data").get("shop").asText());
		textareamap.put("Articles",  input.get("data").get("articles").asText());
		textareamap.put("Date",  input.get("data").get("date").asText());
		textareamap.put("Total", input.get("data").get("total").asText());

		Receipt receipt = new Receipt();
		JsonNode rece = input.get("receipt");
		receipt = mapper.convertValue(rece, Receipt.class);

		JsonObject obj = null;

		
		receipt.getShop().setName(textareamap.get("Shop"));
		textareamap.put("Shop", "Shop:" + textareamap.get("Shop"));
		List<ReceiptEntry> re = TessOCRUtils.parseEntries(textareamap.get("Articles"));
		if (re != null) {
			receipt.setReceiptEntries(re);
		}
		textareamap.put("Articles","Articles:" + textareamap.get("Articles"));
		
		Date d = TessOCRUtils.parseDate(textareamap.get("Date"));
		if (d != null) {
			receipt.setPurchaseDate(d);
		}
		textareamap.put("Date","Date:" + textareamap.get("Date"));
		Integer total = TessOCRUtils.parseTotal(textareamap.get("Total"));
		if (total != null) {
			receipt.setTotalAmount(total);
		}
		textareamap.put("Total", "Total"+textareamap.get("Total"));

		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("ocr", mapper.writeValueAsString(textareamap))
					.add("receipt", mapper.writeValueAsString(receipt)).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result.toString();
	}
	
	

	@RequestMapping(value = "/autoFill", method = RequestMethod.POST)
	@ResponseBody
	public String autoFill(@RequestBody Receipt receipt, Model model, HttpServletRequest request, HttpSession session) {
		String ocr = TessOCRUtils.extractTextFromImage(receipt.getPicture(), request.getSession().getServletContext());

		ObjectMapper mapper = new ObjectMapper();
		JsonObject result = null;
		try {
			result = Json.createObjectBuilder().add("ocr", mapper.writeValueAsString("")).build();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		/*
		 * Customer customer = (Customer) session.getAttribute("customer");
		 * if(customer!=null){ receipt.setCustomer(customer); }
		 * 
		 * ReceiptUtils.checkArticles(receipt); boolean articlesMissing = false;
		 * for(ReceiptEntry re : receipt.getReceiptEntries()){
		 * 
		 * if(re.getArticle().getCategory()==null||re.getArticle().getCategory()
		 * .getName().equals("")){ articlesMissing=true; break; } }
		 * 
		 * if(!articlesMissing){ ReceiptUtils.setCategories(receipt);
		 * if(ReceiptUtils.checkReceiptById(receipt.getId())){
		 * receipt.setReceiptEntries(receipt.getReceiptEntries());
		 * ReceiptUtils.editReceipt(receipt); }else{
		 * ReceiptUtils.addReceipt(receipt); } }
		 * 
		 * ObjectMapper mapper = new ObjectMapper(); JsonObject result = null;
		 * try { result = Json.createObjectBuilder().add("receipt",
		 * mapper.writeValueAsString(receipt)).build(); } catch
		 * (JsonProcessingException e) { e.printStackTrace(); }
		 */

		return result.toString();
	}
}
